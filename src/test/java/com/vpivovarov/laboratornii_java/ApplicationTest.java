package com.vpivovarov.laboratornii_java;

import com.vpivovarov.laboratornii_java.util.LabUtil;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class ApplicationTest {

    @Test
    public void lab1Test() {
        LabUtil.print.accept("Lab 1 - test start");
        Laboratoria1 laboratoria1 = new Laboratoria1(LabUtil.buildList(1D, 1D), "+", 3);
        assertEquals(laboratoria1.result, 2, 0);
        laboratoria1 = new Laboratoria1(LabUtil.buildList(5D, 1D), "*", 3);
        assertEquals(laboratoria1.result, 5, 0);
        laboratoria1 = new Laboratoria1(LabUtil.buildList(10D, 2D), "/", 3);
        assertEquals(laboratoria1.result, 5, 0);
        laboratoria1 = new Laboratoria1(LabUtil.buildList(10D, 2D), "-", 3);
        assertEquals(laboratoria1.result, 8, 0);
        LabUtil.print.accept("Lab 1 - test completed");
    }

    @Test
    public void lab2Test() {
        LabUtil.print.accept("Lab 2 - test start");
        Laboratoria2 laboratoria2 = new Laboratoria2(36D);
        assertEquals(laboratoria2.result, 6, 0);
        laboratoria2 = new Laboratoria2(144D);
        assertEquals(laboratoria2.result, 12, 0);
        laboratoria2 = new Laboratoria2(900D);
        assertEquals(laboratoria2.result, 30, 0);
        laboratoria2 = new Laboratoria2(8100D);
        assertEquals(laboratoria2.result, 90, 0);
        LabUtil.print.accept("Lab 2 - test completed");
    }

    @Test
    public void lab3Test() {
        LabUtil.print.accept("Lab 3 - test start");
        Laboratoria3 laboratoria3 = new Laboratoria3(1, "--poly=1,3,4,s,f,2");
        assertEquals(laboratoria3.result, 2.083333333333333, 0);
        laboratoria3 = new Laboratoria3(2, "--otherParam=121", "--poly=1,3,4,s,f,2");
        assertEquals(laboratoria3.result, 4.166666666666666, 0);
        laboratoria3 = new Laboratoria3(3, "3", "4", "--poly=1,3,4,s,f,2");
        assertEquals(laboratoria3.result, 6.25, 0);
        laboratoria3 = new Laboratoria3(4, "--poly=1,3,4,s,f,2", "--otherParam=12");
        assertEquals(laboratoria3.result, 8.333333333333332, 0);
        LabUtil.print.accept("Lab 3 - test completed");
    }

    @Test
    public void lab4Test() {
        LabUtil.print.accept("Lab 4 - test start");
        Laboratoria4 laboratoria4 = new Laboratoria4(5, 25);
        assertNotNull(laboratoria4.getSma());
        assertEquals(laboratoria4.getSma().getWindowSize(), 5);
        assertEquals(laboratoria4.getSma().getSmaQueue().size(), 5);
        assertTrue(new File("SMA.PNG").exists());
        LabUtil.print.accept("Lab 4 - test completed");
    }

    @Test
    public void lab5Test() {
        LabUtil.print.accept("Lab 5 - test start");
        Laboratoria5 laboratoria5 = new Laboratoria5();
        assertNotNull(laboratoria5.getProcessor());
        assertFalse(laboratoria5.getMails().isEmpty());
        assertFalse(laboratoria5.getMailByAuthor().isEmpty());
        assertFalse(laboratoria5.getSpammers().isEmpty());
        assertTrue(new File("histogram.PNG").exists());
        LabUtil.print.accept("Lab 5 - test completed");
    }
}
