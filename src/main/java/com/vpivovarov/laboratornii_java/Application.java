package com.vpivovarov.laboratornii_java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
    private static final Logger LOG = LoggerFactory.getLogger(Application.class);
    private static final String LAB_TASK_TEMPLATE = "Выполнение лабораторного задание № {}";

    public static void main(String[] args) {
        LOG.info(LAB_TASK_TEMPLATE, 1);
        Laboratoria1 laboratoria1 = new Laboratoria1();
        LOG.info(LAB_TASK_TEMPLATE, 2);
        Laboratoria2 laboratoria2 = new Laboratoria2();
        LOG.info(LAB_TASK_TEMPLATE, 3);
        Laboratoria3 laboratoria3 = new Laboratoria3();
        LOG.info(LAB_TASK_TEMPLATE, 4);
        Laboratoria4 laboratoria4 = new Laboratoria4();
        LOG.info(LAB_TASK_TEMPLATE, 5);
        Laboratoria5 laboratoria5 = new Laboratoria5();
    }
}
