package com.vpivovarov.laboratornii_java;

import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.vpivovarov.laboratornii_java.util.LabUtil.print;

@Data
class Laboratoria4 {
    private final Sma sma;

    Laboratoria4() {
        sma = new Sma();
        sma.setWindowSize(15);
        sma.start(15);
    }

    Laboratoria4(int windowSize, int iteration) {
        sma = new Sma();
        sma.setWindowSize(windowSize);
        sma.start(iteration);
    }

    @Data
    public static class Sma {
        private LinkedList<Pair<Long, Double>> queue = new LinkedList<>();
        private LinkedList<Double> smaQueue = new LinkedList<>();
        private int windowSize;

        void start(int n) {
            Random random = new Random();
            for (double i : List.of(5.3D, 6.2D, 8D, 7.2D, 5.3D, 4D, 3.5D, 6.2D, 7D, 9D, 8D, 6.6D, 8.3D, 9D, 7.5D)) {
                push(i);
                sleep(1000);

                if (queue.size() == windowSize) {
                    calc_sma();
                    saveChart();
                    print.accept("Chart saved!\n");
                }
            }
        }

        private void saveChart() {
            try {
                DefaultCategoryDataset dataset = new DefaultCategoryDataset();
                queue.stream().map(Pair::getValue)
                        .forEach(i -> dataset.addValue(i, "data", "data"));
                JFreeChart chart = ChartFactory.createLineChart("Sma", null, null,
                        dataset, PlotOrientation.VERTICAL, false, false, false);

                ChartUtils.saveChartAsPNG(new File("SMA.PNG"), chart, 500, 500);
            } catch (IOException ex) {
                print.accept(ex.getMessage());
            }
        }

        private void push(Double item) {
            queue.addFirst(Pair.of(System.currentTimeMillis(), item));
            if (queue.size() > windowSize) {
                queue.pollLast();
            }
            print.accept("Введите число: " + item);
        }

        private void calc_sma() {
            double mean = mean();
            smaQueue = queue.stream().map(i -> (i.getValue() - mean) / queue.size())
                    .collect(Collectors.toCollection(LinkedList::new));

        }

        private double mean() {
            return queue.stream().map(Pair::getValue).reduce(0D, Double::sum) / queue.size();
        }
    }

    private static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            print.accept(ex.getMessage());
        }
    }
}
