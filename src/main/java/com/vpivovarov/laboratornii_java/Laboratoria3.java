package com.vpivovarov.laboratornii_java;

import lombok.Getter;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.vpivovarov.laboratornii_java.util.LabUtil.print;

class Laboratoria3 {
    private static final String POLYNOM_PARAM = "--poly=";
    double result;

    Laboratoria3() {
        print.accept("Список аргументов: --poly=1,2,s,2,dc,12");
        Polynom polynom = new Polynom(1, "--poly=1,2,s,2,dc,12");
        result = polynom.calc();
        print.accept("Результат " + result);
    }

    Laboratoria3(double constant, String... args) {
        Polynom polynom = new Polynom(constant, args);
        result = polynom.calc();
        print.accept("Результат " + result);
    }

    @Getter
    private class Polynom {
        private double constant;
        private List<Double> elements;

        Polynom(double constant, String... args) {
            this.constant = constant;
            this.elements = parseInput(args);
        }

        double calc() {
            return elements.stream().mapToDouble(i -> 1 / i * constant).sum();
        }

        private List<Double> parseInput(String... args) {
            List<Double> result = new ArrayList<>();
            if (args.length != 0) {
                Stream.of(args).filter(arg -> arg.startsWith(POLYNOM_PARAM)).findFirst().ifPresent(polyArg ->
                        result.addAll(filterData(polyArg.replaceAll(POLYNOM_PARAM, "").split(","))));
            }

            return result;
        }

        private List<Double> filterData(String... args) {
            return Stream.of(args).filter(NumberUtils::isDigits).map(Double::parseDouble).collect(Collectors.toList());
        }
    }
}
