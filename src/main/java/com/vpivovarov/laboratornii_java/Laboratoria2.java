package com.vpivovarov.laboratornii_java;

import java.util.Scanner;

import static com.vpivovarov.laboratornii_java.util.LabUtil.print;

class Laboratoria2 {
    private static final Scanner INPUT = new Scanner(System.in);
    double result;

    Laboratoria2() {
        Sqrt sqrt = new Sqrt();
        result = sqrt.calc();
        print.accept("Результат " + result + "\n");
    }

    Laboratoria2(Double num) {
        Sqrt sqrt = new Sqrt(num);
        result = sqrt.calc();
        print.accept("Результат " + result + "\n");
    }


    private static class Sqrt {
        private Double num;

        Sqrt() {
            print.accept("Введите число: ");
            this.num = INPUT.nextDouble();
        }

        Sqrt(Double num) {
            print.accept("Введите число: " + num);
            this.num = num;
        }

        private Double calc() {
            Double prev = null;
            Double x = 1d;

            while (true) {
                x = 0.5 * (x + num / x);
                if (prev != null && prev.equals(x)) {
                    return x;
                }
                prev = x;
            }
        }
    }
}
