package com.vpivovarov.laboratornii_java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class LabUtil {
    public static Consumer<String> print = msg -> System.out.print("\n" + msg + " ");

    public static <T> List<T> buildList(T... values) {
        return new ArrayList<>(Arrays.asList(values));
    }
}
